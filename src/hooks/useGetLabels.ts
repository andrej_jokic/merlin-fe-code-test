import { ListLabelsData, LIST_LABELS } from "../queries";
import useRequest, { GqlResponse, UseRequestHooks } from "./useRequest";

import Label from "../models/Label";
import RootQuery from "../models/RootQuery";

const updateCache = (res: GqlResponse<ListLabelsData>) => {
  const labels = res.data?.labels || [];
  const labelRefs = Label.writeObjects(labels);
  RootQuery.writeList("labels", labelRefs);
};

const useGetLabels = (): UseRequestHooks<ListLabelsData> => {
  return useRequest<ListLabelsData>(LIST_LABELS, updateCache);
};

export default useGetLabels;
