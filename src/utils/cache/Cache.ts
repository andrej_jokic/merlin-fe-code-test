import { useEffect, useReducer, useRef } from "react";

class Cache {
  protected cache: CacheObject = {};
  protected handlers: CacheHandler = {};

  public subscribe(ids: string[], fn: () => void): void {
    ids.forEach((id) => {
      this.handlers[id] = [...(this.handlers[id] || []), fn];
    });
  }

  public unsubscribe(ids: string[], fn: () => void): void {
    ids.forEach((id) => {
      this.handlers[id] = (this.handlers[id] || []).filter((v) => v !== fn);
    });
  }

  public broadcastUpdate(ids: string[]): void {
    ids.map((id) => (this.handlers[id] || []).forEach((fn) => fn()));
  }

  public read(ids: string[]): CacheItem[] {
    return ids.map((id) => this.cache[id]);
  }

  public write(ids: string[], values: CacheItem[]): void {
    ids.forEach((id, i) => (this.cache[id] = values[i]));
    this.broadcastUpdate(ids);
  }
}

const cache = new Cache();

export const useCacheUpdateSubscription = (ids: string[]): void => {
  const [, forceRefresh] = useReducer((s) => s + 1, 0);

  useEffect(() => {
    const handler = () => forceRefresh();
    cache.subscribe(ids, handler);
    return () => cache.unsubscribe(ids, handler);
  }, [ids]);
};

export type CacheItem =
  | CacheItem[]
  | CacheObject
  | CacheObject[]
  | string
  | string[]
  | number
  | number[]
  | null;

type CacheObject = {
  [key: string]: CacheItem;
};

type CacheHandler = {
  [key: string]: (() => void)[];
};

export default cache;
