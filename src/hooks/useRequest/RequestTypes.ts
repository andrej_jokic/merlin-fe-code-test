import { GqlResponse } from "../../utils";

export type QueryVars = Record<string, unknown> | undefined;

export type UpdateFunction<TData, TVars> = (
  res: GqlResponse<TData>,
  variables: TVars | undefined
) => void;

export interface UseRequest {
  <TData, TVars extends QueryVars = undefined>(
    query: string,
    updateFn?: UpdateFunction<TData, TVars>
  ): UseRequestHooks<TData, TVars>;
}

export type UseRequestHooks<TData, TVars extends QueryVars = undefined> = [
  FetchDataFunc<TData, TVars>,
  QueryState
];

export type QueryState = {
  loading: boolean;
  error?: string;
};

export type QueryAction = { type: ActionType, payload?: unknown };

export interface QueryReducer {
  (state: QueryState, action: QueryAction): QueryState;
}

export interface FetchDataFunc<TData, TVars extends QueryVars = undefined> {
  (variables?: TVars): Promise<GqlResponse<TData>>;
}

export enum ActionType {
  REQUEST,
  SUCCESS,
  ERROR,
}
