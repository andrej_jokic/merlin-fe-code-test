export const LIST_LABELS = `
  query labels {
    labels {
      id
      name
    }
  }`;

export type Label = {
  id: string;
  name: string;
};

export type ListLabelsData = {
  labels: Label[];
};

export const GET_LABEL = `
  query label($id: ID!) {
    label(id: $id) {
      id
      name
    }
  }`;

export type GetLabelData = {
  label?: Label;
};

export type GetLabelVars = {
  id: string;
};

export const CREATE_LABEL = `
  mutation createLabel($data: LabelInput!) {
    createLabel(data: $data) {
      id
      name
    }
  }`;

export type CreateLabelData = {
  createLabel: Label;
};

export type CreateLabelVars = {
  data: {
    name: string;
  };
};

export const UPDATE_LABEL = `
  mutation updateLabel($id: ID!, $data: LabelInput!) {
    updateLabel(id: $id, data: $data) {
      id
      name
    }
  }`;

export type UpdateLabelData = {
  updateLabel: Label;
};

export type UpdateLabelVars = {
  id: string;
  data: {
    name: string;
  };
};

export const DELETE_LABEL = `
  mutation deleteLabel($id: ID!) {
    deleteLabel(id: $id)
  }`;

export type DeleteLabelData = {
  deleteLabel: boolean;
};

export type DeleteLabelVars = {
  id: string;
};
