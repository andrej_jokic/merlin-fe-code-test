import { DeleteLabelData, DeleteLabelVars, DELETE_LABEL } from "../queries";
import useRequest, { UseRequestHooks } from "./useRequest";

import RootQuery from "../models/RootQuery";
import Label from "../models/Label";
import { useCallback } from "react";

const useDeleteLabel = (): UseRequestHooks<
  DeleteLabelData,
  DeleteLabelVars
> => {
  const oldLabels = RootQuery.useCacheValue("labels") as string[];

  return useRequest<DeleteLabelData, DeleteLabelVars>(
    DELETE_LABEL,
    useCallback(
      (res, variables) => {
        const deleted = res.data?.deleteLabel;
        if (!deleted || !variables) return;

        const ref = Label.getCacheId(variables.id);

        RootQuery.writeList(
          "labels",
          oldLabels.filter((label) => label !== ref)
        );
      },
      [oldLabels]
    )
  );
};

export default useDeleteLabel;
