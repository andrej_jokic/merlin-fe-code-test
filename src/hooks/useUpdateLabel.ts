import { UpdateLabelData, UpdateLabelVars, UPDATE_LABEL } from "../queries";
import useRequest, { UseRequestHooks, GqlResponse } from "./useRequest";

import Label from "../models/Label";

const updateCache = (res: GqlResponse<UpdateLabelData>) => {
  const label = res.data?.updateLabel;
  if (!label) return;

  Label.writeObject(label);
};

const useUpdateLabel = (): UseRequestHooks<
  UpdateLabelData,
  UpdateLabelVars
> => {
  return useRequest<UpdateLabelData, UpdateLabelVars>(
    UPDATE_LABEL,
    updateCache
  );
};

export default useUpdateLabel;
