import React from "react";
import Button, { ButtonTypes } from "../Button/Button";
import styles from "./ListItem.module.css";

interface Props {
  text: string;
  onClick: (event: React.MouseEvent) => void;
  isSelected: boolean;
}

const ListItem: React.FunctionComponent<Props> = ({
  text,
  onClick,
  isSelected,
}: Props) => {
  return (
    <li className={`${styles.item} ${isSelected ? styles.isSelected : ""}`}>
      <Button onClick={onClick} buttonType={ButtonTypes.clear}>
        {text}
      </Button>
    </li>
  );
};

export default ListItem;
