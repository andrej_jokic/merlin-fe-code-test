import React from "react";
import { Label, ListLabelsData } from "../../../queries";
import ListItem from "../../shared/ListItem/ListItem";

interface Props {
  labels: ListLabelsData["labels"];
  selectedLabel: Label | null;
  setSelectedLabel: (label: Label) => void;
}

const LabelsList: React.FunctionComponent<Props> = ({
  labels,
  selectedLabel,
  setSelectedLabel,
}: Props) => {
  return (
    <ul>
      {labels.map((label) => (
        <ListItem
          onClick={() => setSelectedLabel(label)}
          key={label.id}
          text={label.name}
          isSelected={label.id === selectedLabel?.id}
        />
      ))}
    </ul>
  );
};

export default LabelsList;
