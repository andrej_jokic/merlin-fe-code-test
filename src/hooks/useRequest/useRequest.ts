import { useCallback, useReducer } from "react";
import { gqlFetch } from "../../utils";
import {
  UseRequest,
  QueryReducer,
  QueryState,
  QueryAction,
  FetchDataFunc,
  QueryVars,
  ActionType,
  UpdateFunction,
} from "./RequestTypes";

/* 
    CHALLENGE:

    Modify to return an error state in fetchState.
    
*/

const fetchReducer: QueryReducer = (state: QueryState, action: QueryAction) => {
  switch (action.type) {
    case ActionType.REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ActionType.SUCCESS:
      return {
        ...state,
        loading: false,
        error: undefined,
      };
    case ActionType.ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload as string,
      };
    default:
      return state;
  }
};

const actions = {
  fetch: () => ({ type: ActionType.REQUEST }),
  loaded: () => ({ type: ActionType.SUCCESS }),
  error: (message: string) => ({ type: ActionType.ERROR, payload: message }),
};

const useRequest: UseRequest = <TData, TVars extends QueryVars = undefined>(
  query: string,
  updateFn?: UpdateFunction<TData, TVars>
) => {
  const [fetchState, dispatch] = useReducer<QueryReducer>(fetchReducer, {
    loading: false,
  });

  const fetchData: FetchDataFunc<TData, TVars> = useCallback(
    (variables?: TVars) => {
      dispatch(actions.fetch());

      return gqlFetch<TData, TVars>(query, variables)
        .then((res) => {
          dispatch(actions.loaded());

          if (res.errors) {
            const message = res.errors
              .map(({ message }: { message: string }) => message)
              .join(",");

            return Promise.reject(new Error(message));
          }

          if (updateFn) updateFn(res, variables);
          return Promise.resolve(res);
        })
        .catch((reason) => {
          dispatch(actions.error(reason.message));
          return Promise.reject({ error: reason });
        });
    },
    [query, updateFn]
  );

  return [fetchData, fetchState];
};

export default useRequest;
