import { Model } from "../utils";

class RootQuery extends Model {
  static typename = "Root";
}

export default RootQuery;
