# Merlin Frontend Code Test

### Introduction

This project's (shared) state management uses a system inspired by Apollo 3's InMemoryCache. While you don't have to interact directly with it for the challenge, it is designed to throw you off.

Everything you need to do can be implemented entirely using standard React Hooks.

If you have no typescript expereience, feel free to rename any files you modify to have `.js` or `.jsx` extensions, and remove the typings. This won't affect any code functionality.

Further instructions can be found in `src/app.tsx`.

If you have any notes or feedback about the test, feel free to leave them below.

### Installation

##### Install Dependencies

`npm i `

##### Start Backend

`npm run backend`

##### Run (Dev Mode)

`npm start`

---

##### Applicant Notes:
