import React from "react";
import styles from "./Input.module.css";

type InputProps = React.InputHTMLAttributes<HTMLInputElement>;

const Input: React.FunctionComponent<InputProps> = ({
  ...props
}: InputProps) => {
  return <input className={styles.input} {...props} />;
};

export default Input;
