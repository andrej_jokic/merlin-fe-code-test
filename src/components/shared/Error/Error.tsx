import React from "react";
import styles from "./Error.module.css";

interface Props {
  error?: string;
}

const Error: React.FunctionComponent<Props> = ({ error }: Props) => {
  return error ? <p className={styles.message}>{error}</p> : null;
};

export default Error;
