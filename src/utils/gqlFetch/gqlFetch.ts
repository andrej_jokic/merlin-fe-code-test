export type GqlResponse<TData> = { data?: TData; errors?: Array<{message: string}> };

const gqlFetch = async <TData, TVars>(
  query: string,
  variables?: TVars
): Promise<GqlResponse<TData>> => {
  const uri = process.env.API_URL as string;

  const res = await fetch(uri, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ query: query.trim(), variables }),
  });

  return await res.json();
};

export default gqlFetch;
