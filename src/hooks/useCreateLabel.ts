import { CreateLabelData, CreateLabelVars, CREATE_LABEL } from "../queries";
import useRequest, { UseRequestHooks, GqlResponse } from "./useRequest";

import Label from "../models/Label";
import RootQuery from "../models/RootQuery";
import { useCallback } from "react";

const useCreateLabel = (): UseRequestHooks<
  CreateLabelData,
  CreateLabelVars
> => {
  const oldLabels = RootQuery.useCacheValue("labels") as string[];

  const updateCache = useCallback(
    (res: GqlResponse<CreateLabelData>) => {
      const label = res.data?.createLabel;
      if (!label) return;

      const newLabel = Label.writeObject(label);
      RootQuery.writeList("labels", [...(oldLabels || []), newLabel]);
    },
    [oldLabels]
  );

  return useRequest<CreateLabelData, CreateLabelVars>(
    CREATE_LABEL,
    updateCache
  );
};

export default useCreateLabel;
