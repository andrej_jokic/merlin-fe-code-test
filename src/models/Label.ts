import { Model } from "../utils";

class Label extends Model {
  static typename = "Label";
}

export default Label;
