import gqlFetch from "./gqlFetch";

export { default } from "./gqlFetch";

export type { GqlResponse } from "./gqlFetch";
