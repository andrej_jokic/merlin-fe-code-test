import express from "express";
import { graphqlHTTP } from "express-graphql";
import cors from "cors";

import { typeDefs, resolvers } from "./graphql";

import { makeExecutableSchema } from "@graphql-tools/schema";

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

const app = express();

app.use(cors());

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);

app.listen(4000, () => {
  console.info("Listening on http://localhost:4000/graphql");
});
