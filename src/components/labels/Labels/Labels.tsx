import React, { useEffect, useState } from "react";
import { useGetLabels } from "../../../hooks";
import { Label, RootQuery } from "../../../models";
import { ListLabelsData, Label as LabelType } from "../../../queries";
import Dialog from "../../shared/Dialog/Dialog";
import LabelsForm from "../LabelsForm/LabelsForm";
import LabelsList from "../LabelsList/LabelsList";
import styles from './Labels.module.css';
import Error from "../../shared/Error/Error";
import Loader from "../../shared/Loader/Loader";

const Labels: React.FunctionComponent = () => {
  const labelRefs = (RootQuery.useCacheValue("labels") as string[]) ?? [];
  const labels = Label.useCacheValues(labelRefs) as ListLabelsData["labels"];

  const [getLabels, { loading, error }] = useGetLabels();

  useEffect(() => {
    getLabels();
  }, [getLabels]);

  const [selectedLabel, setSelectedLabel] = useState<LabelType | null>(null);

  return (
    <Dialog>
      <p className={styles.title}>Music Labels</p>
      <Error error={error} />
      <Loader isLoading={loading}>
        <LabelsList
          selectedLabel={selectedLabel}
          setSelectedLabel={setSelectedLabel}
          labels={labels}
        />
      </Loader>
      
      <LabelsForm
        selectedLabel={selectedLabel}
        setSelectedLabel={setSelectedLabel}
      />
    </Dialog>
  );
};

export default Labels;
