import React from "react";

interface Props {
  isLoading: boolean;
  children: React.ReactNode;
}

const Loader: React.FunctionComponent<Props> = ({
  isLoading,
  children,
}: Props) => {
  return isLoading ? <div>Loading...</div> : <>{ children }</>;
};

export default Loader;
