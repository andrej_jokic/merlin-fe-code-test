import React from 'react';
import styles from './AppContainer.module.css';

interface Props {
  children: React.ReactNode;
}

const AppContainer: React.FunctionComponent<Props> = ({ children }: Props) => {
  return (
    <div className={styles.container}>
      { children }
    </div>
  )
}

export default AppContainer
