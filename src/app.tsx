import React from "react";
import { render } from "react-dom";
import Labels from "./components/labels/Labels/Labels";
import AppContainer from "./components/shared/AppContainer/AppContainer";
import './app.css';

/* 

  Challenges:

  a)  Add styling match the image 'Design to Match.png' in the root. Feel free to componentise any display elements.

      Colors: 
        Background - gradient #3498db #2980b9
        Input - #f2f2f2
        Button - #1e90ff

      Fonts:
        Roboto (obtainable from Google Fonts)

  b)  Add full CRUD functionality for Labels using provided hooks. I've added  a basic 'create' as an example, but
      feel free to replace this if you prefer.

  c)  Implement a basic error handler on the useRequest hook. This should trickle down a stateful error message to 
      the CRUD hooks below (you should also display this appropriately on the form)


  Note:

  A GraphQL error can come in two forms. Assume a network error will cause the 'gqlFetch' util to throw, but an error
  should also be reported if the JSON response contains an 'errors' field.

  i.e. 

  HTTP 200 
  {
    "data": { ... },
    "errors": [{ "message": "This is an error", ... }]
  }

  is still an error.

*/

const App = () => {
  return (
    <AppContainer>
      <Labels />
    </AppContainer>
  );
};

render(<App />, document.getElementById("root"));
