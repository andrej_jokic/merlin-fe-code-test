import React from "react";
import styles from "./Button.module.css";

export enum ButtonTypes {
  primary,
  secondary,
  danger,
  clear,
}

const ButtonStyles: Record<ButtonTypes, string> = {
  [ButtonTypes.primary]: styles.primary as string,
  [ButtonTypes.secondary]: styles.secondary as string,
  [ButtonTypes.danger]: styles.danger as string,
  [ButtonTypes.clear]: styles.clear as string,
};

interface Props {
  children: React.ReactNode;
  className?: string;
  buttonType: ButtonTypes;
}

type ButtonProps = Props & React.ButtonHTMLAttributes<HTMLButtonElement>;

const Button: React.FunctionComponent<ButtonProps> = ({
  children,
  buttonType,
  className,
  ...props
}: ButtonProps) => {
  return (
    <button
      role="button"
      className={`${styles.button} ${ButtonStyles[buttonType] || ''} ${className || ''}`}
      {...props}
    >
      {children}
    </button>
  );
};

export default Button;
