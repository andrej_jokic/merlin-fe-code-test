import React, { useEffect, useState } from "react";
import { useCreateLabel, useDeleteLabel, useUpdateLabel } from "../../../hooks";
import { Label } from "../../../queries";
import Button, { ButtonTypes } from "../../shared/Button/Button";
import Error from "../../shared/Error/Error";
import Input from "../../shared/Input/Input";

interface Props {
  selectedLabel: Label | null;
  setSelectedLabel: (label: Label | null) => void;
}

const LabelsForm: React.FunctionComponent<Props> = ({
  selectedLabel,
  setSelectedLabel,
}: Props) => {
  const [
    addLabel,
    { loading: createLoading, error: createError },
  ] = useCreateLabel();
  const [
    deleteLabel,
    { loading: deleteLoading, error: deleteError },
  ] = useDeleteLabel();
  const [
    updateLabel,
    { loading: updateLoading, error: updateError },
  ] = useUpdateLabel();

  const [labelName, setLabelName] = useState<string>("");

  const handleAdd = async () => {
    await addLabel({ data: { name: labelName } });
    setLabelName("");
  }

  const handleUpdate = async () => {
    if (!selectedLabel) return;
    await updateLabel({ id: selectedLabel.id, data: { name: labelName } })
    setSelectedLabel(null);
  }

  const handleDelete = async () => {
    if (!selectedLabel) return;
    await deleteLabel({ id: selectedLabel.id });
    setSelectedLabel(null);
  }

  useEffect(() => {
    if (selectedLabel) setLabelName(selectedLabel.name);
    else setLabelName("");
  }, [selectedLabel]);

  return (
    <>
      <Error error={createError || deleteError || updateError} />
      <Input
        value={labelName}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setLabelName(e.target.value)
        }
        placeholder="Enter new Label"
        aria-label="Label name"
      />
      {selectedLabel ? (
        <>
          <Button
            onClick={handleUpdate}
            disabled={selectedLabel.name === labelName || updateLoading}
            buttonType={ButtonTypes.primary}
          >
            Update
          </Button>
          <Button
            onClick={handleDelete}
            disabled={deleteLoading}
            buttonType={ButtonTypes.danger}
          >
            Delete
          </Button>
          <Button
            onClick={() => setSelectedLabel(null)}
            disabled={createLoading}
            buttonType={ButtonTypes.secondary}
          >
            Cancel
          </Button>
        </>
      ) : (
        <Button
          onClick={handleAdd}
          disabled={createLoading || !labelName}
          buttonType={ButtonTypes.primary}
        >
          Add
        </Button>
      )}
    </>
  );
};

export default LabelsForm;
