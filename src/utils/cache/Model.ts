/* eslint-disable react-hooks/rules-of-hooks */

import { useMemo } from "react";
import cache, { CacheItem, useCacheUpdateSubscription } from "./Cache";

export type Cacheable = CacheItem & { id: string };

abstract class Model {
  static typename: string;

  public static useCacheValues(ids: string[]): CacheItem[] {
    const refs = useMemo(() => this.getCacheIds(ids), [ids]);
    useCacheUpdateSubscription(refs);

    return cache.read(refs);
  }

  public static useCacheValue(id: string): CacheItem {
    const ids = useMemo(() => [id], [id]);

    return this.useCacheValues(ids)[0];
  }

  public static writeList(name: string, list: CacheItem[]): string {
    const ref = this.getCacheId(name);

    cache.write([ref], [list]);
    cache.broadcastUpdate([ref]);

    return ref;
  }

  public static writeObject(val: Cacheable): string {
    const ref = this.getCacheId(val.id);

    cache.write([ref], [val]);
    cache.broadcastUpdate([ref]);

    return ref;
  }

  public static writeObjects(vals: Cacheable[]): string[] {
    const refs = this.getCacheIds(vals.map((v) => v.id));

    cache.write(refs, vals);
    cache.broadcastUpdate(refs);

    return refs;
  }

  public static getCacheId(id: string): string {
    return id.includes(`${this.typename}:`) ? id : `${this.typename}:${id}`;
  }

  public static getCacheIds(ids: string[]): string[] {
    return ids.map((id) => this.getCacheId(id));
  }
}

export default Model;
