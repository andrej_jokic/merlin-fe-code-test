export const typeDefs = `
type Query {
    label(id: Int!): Label
    labels: [Label!]!
}

type Mutation {
    createLabel(data: LabelInput): Label!
    updateLabel(id: ID!, data: LabelInput): Label
    deleteLabel(id: ID!): Boolean!
}

type Label {
    id: ID!
    name: String!
}

input LabelInput {
  name: String!
}
`;

type LabelData = {
  data: {
    name: string;
  };
};

type Label = {
  id: string;
} & LabelData["data"];

const labels: Label[] = [
  {
    id: "1",
    name: "XL Recordings",
  },
  {
    id: "2",
    name: "Sub Pop",
  },
];

const nextId = () =>
  `${
    labels.reduce(
      (highest, { id }) => (highest > parseInt(id) ? highest : parseInt(id)),
      1
    ) + 1
  }`;

export const resolvers = {
  Query: {
    labels: (): Label[] => labels,
    label: (_, { id }: { id: string }): Label =>
      labels.find((l) => l.id === id),
  },

  Mutation: {
    createLabel: (_, { data }: LabelData): Label => {
      labels.push({ ...data, id: nextId() });
      return labels[labels.length - 1];
    },
    updateLabel: (_, { id, data }: LabelData & { id: string }): Label => {
      const index = labels.findIndex((l) => l.id === id);
      if (index < 0) throw new Error("Cannot find label with id " + id);

      labels[index] = { ...data, id };
      return labels[index];
    },
    deleteLabel: (_, { id }: { id: string }): boolean => {
      const index = labels.findIndex((l) => l.id === id);
      if (index < 0) throw new Error("Cannot find label with id " + id);

      labels.splice(index, 1);
      return true;
    },
  },
};
